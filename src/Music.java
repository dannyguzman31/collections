public class Music {

    public int id;
    public String Album, Artist, Song;
    public int year;

    public Music(int id, String Album, String Artist, String Song, int year){
        this.id = id;
        this.Album = Album;
        this.Artist = Artist;
        this.Song = Song;
        this.year = year;
    }

}
