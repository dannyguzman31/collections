import java.util.*;

public class JavaCollections {
    public static void main(String[] args){

    /*
        LIST: Can contain duplicates and elements are ordered, examples
        are LinkedList and ArrayList
     */
    System.out.println("--- LIST ---");
    ArrayList arr = new ArrayList(); // create an ArrayList Object
    arr.add(1); // Add values to the array
    arr.add(2);
    arr.add(3);
    arr.add(4);
    System.out.println("Vector: " + arr); // Print the array
    Vector v = new Vector(); // Create an vector object
    v.add(0); // add a Element to the vector
    v.addAll(1, arr); // Copy all the elements of the array
    System.out.println("New Vector: " + v); // print new vector
    v.clear(); // Clears the whole vector
    System.out.println("New Vector: " + v);

    /*
        SET: A set is a collection but can not have any duplicates values,
        if it finds a duplicate, it will remove it
     */
    System.out.println("--- SET ---");
        // We create an array of strings
        String[] cities = {"Los Angeles", "Miami", "Boston", "Chicago", "Miami", "Houston", "Boston", "Los Angeles" };
        // Create an object list of array
        List<String> list = Arrays.asList(cities);
        // Print out the array
        System.out.printf("%s", list);
        System.out.println();
        // we create an object set from the HashSet class
        Set<String> set = new HashSet<String>(list);
        // The HashSet class removes all the duplicates and prints out the new array
        System.out.printf("%s", set);
        System.out.println();

    /*
        QUEUE: Is used to insert elements at the end of the queue and
        it removes from the beginning, FIFO (First In, First Out)
     */
    System.out.println("--- QUEUE ---");
        // Create a Queue object
        PriorityQueue<String> q = new PriorityQueue<>();
        // To add an object to the queue, we use the offer method
        q.offer("1");
        q.offer("2");
        q.offer("3");
        q.offer("4");
        q.offer("5");

        System.out.printf("%s", q); // print out the queue
        System.out.println();

        String head = q.peek(); // creates the element with highest priority (head)
        q.poll(); // removes the element with highest priority (head)

        System.out.printf("%s", q); // new queue
        System.out.println();
    /*
        DEQUE: Elements can be inserted and removed at both ends (Head and end)
        It follows a both FIFO and LIFO
     */
    System.out.println("--- DEQUE ---");
        // Create a deque object
        Deque deque = new LinkedList();
        // add elements to the deque
        deque.add("Element 0");
        deque.add("Element 1");
        deque.add("Element 2");
        // Use an iterator to loop through the deque
        Iterator iterator = deque.iterator();
        while(iterator.hasNext()){
            String element = (String) iterator.next();
        }
        System.out.println("Deque:" + deque); // print out the deque
        deque.pop(); // Removes element from the head and returns it
        deque.push("Element -1");// add an element at the head of the queue
        System.out.println("New Deque:" + deque); //Returns new deque
        deque.removeFirst(); // removes the element at the head
        deque.removeLast(); // removes the element at the end
        System.out.println("Final Deque: " + deque);
    /*
        MAP : Contains key pairs and value pairs, it does not allow
        duplicates. Examples are HashMap and TreeMap
     */

    System.out.println("--- MAP ---");
        // Create a map object
        Map map = new HashMap();
        // Add an element using a key and value pair values
        map.put(1,"Robert");
        map.put(2,"James");
        map.put(3,"John");
        map.put(4,"Daniel");
        map.put(5,"Tim");
        // Use a key iterator to loop through the map
        Iterator iterator1 = map.keySet().iterator();
        while (iterator1.hasNext()){
            Object key = iterator1.next(); // get element from key
            Object value = map.get(key); // get element from value
        }
        System.out.println("Map: " + map); // print out the Map
        map.remove(1); // removes key 1 from Map
        map.replace(2, "William"); // replaces key 2 with a different value
        System.out.println("New Map: " + map); // print out new Map


        System.out.println("-------- Generics using Map --------");

        Map<Integer, String> myMap = new HashMap<Integer, String>();

        myMap.put(1, "Liliana");
        myMap.put(2, "Andy");
        myMap.put(3, "Chelsea");
        myMap.put(4, "Mia");

        Set<Map.Entry<Integer, String>> mySet = myMap.entrySet();

        Iterator<Map.Entry<Integer, String>> myItr = mySet.iterator();

        while(myItr.hasNext()){
            Map.Entry<Integer, String> i = myItr.next();
            System.out.println(i.getKey() + " " + i.getValue());
        }

        System.out.println();
        System.out.println("---------------------------------------------------------------------------------------");
        // Music PlayList using ArrayList

        System.out.println("Music Playlist: ");
        List<Music> myList = new ArrayList<Music>();//Create list of music
        // Create Music
        Music m1 = new Music(1,"And Justice For All", "Metallica", "One", 1988);
        Music m2 = new Music(2,"Fear of the Dark", "Iron Maiden", "Fear of the Dark", 1992);
        Music m3 = new Music(3,"The Razors Edge", "AC/DC", "Thunderstruck", 1990);
        Music m4 = new Music(4,"Appetite For Destruction", "Guns N' Roses", "Sweet Child O' Mine", 1987);
        Music m5 = new Music(5,"Aerosmith", "Aerosmith", "Dream On", 1973);


        // add Music list
        myList.add(m1);
        myList.add(m2);
        myList.add(m3);
        myList.add(m4);
        myList.add(m5);
        // loop through and print out
        for( Music music: myList){
            System.out.println( music.id + " " + music.Album + " " + music.Artist + " " + music.Song + " " + music.year);
        }

    }
}
